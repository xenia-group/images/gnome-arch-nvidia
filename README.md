# GNOME arch

GNOME Arch linux image with added Nvidia drivers.

**OS:** Arch Linux

**DE:** GNOME

**Init system:** systemd

## Details

`./overlay` - Filesystem overlay

- `overlay/local` - Overlay for the specific image
- `overlay/overlay` - [Global overlay on all Nvidia images](https://gitlab.com/xenia-group/images/common/overlay-nvidia)

`./global` - [Global Arch Nvidia config](https://gitlab.com/xenia-group/images/common/global-arch-nvidia)

- `fsscript.sh` - Script that runs at end of image creation
- `package.list` - Default package list

`./` - Local settings

- `fsscript.sh` - Script that runs at end of image creation, after global fsscript
- `package.list` - Additional packages for the specific image
